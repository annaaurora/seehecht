# seehecht 🐟

[![asciicast showcasing seehecht](https://asciinema.org/a/499653.svg)](https://asciinema.org/a/499653)

A tool to quickly open a markdown document with already filled out frontmatter.

## Installation

### Prerequisites

seehecht should work on any system with a shell that supports running binaries.

### Packages

| Operating System                                     | Package Manager  | Package                     | Command                                                                           |
| ---------------------------------------------------- | ---------------- | --------------------------- | --------------------------------------------------------------------------------- |
| [Various][rust-platforms]                            | [Cargo][cargo]   | [seehecht][seehecht-crate]  | `cargo install seehecht`                                                          |
| [Arch Linux][arch linux]                             | [pacman][pacman] | [seehecht][seehecht-pacman] | `git clone https://aur.archlinux.org/seehecht.git && cd seehecht && makepkg -sri` |
| [NixOS][nixos], [Linux][nix-plat], [macOS][nix-plat] | [Nix][nix]       | [seehecht][seehecht-nixpkg] | `nix-env -iA nixos.seehecht` or `nix-env -iA nixpkgs.seehecht`                    |

[rust-platforms]: https://forge.rust-lang.org/release/platform-support.html
[cargo]: https://www.rust-lang.org
[seehecht-crate]: https://crates.io/crates/seehecht
[arch linux]: https://www.archlinux.org
[pacman]: https://wiki.archlinux.org/title/Pacman
[seehecht-pacman]: https://aur.archlinux.org/packages/seehecht
[nixos]: https://nixos.org/nixos/
[nix-plat]: https://nixos.org/nix/manual/#ch-supported-platforms
[nix]: https://nixos.org/nix/
[seehecht-nixpkg]: https://github.com/NixOS/nixpkgs/blob/master/pkgs/tools/misc/seehecht/default.nix

![package version table](https://repology.org/badge/vertical-allrepos/seehecht.svg)

## Running

See [the installation section](#installation) for how to install seehecht on your computer.

Note: `seehecht` does not work if you installed it via cargo:
- Run seehecht with `seh` or `seehecht`.
- Use `seh --help` or `seehecht --help` to look up how to pass options to seehecht.

## License

©️ 2022 papojari <mailto:papojari-git.ovoid@aleeas.com> <https://matrix.to/#/@papojari:artemislena.eu> <https://papojari.codeberg.page>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License version 3 along with this program. If not, see <https://www.gnu.org/licenses/>.

